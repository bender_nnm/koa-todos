const mongoose = require('mongoose');
const db = mongoose.createConnection(process.env.DB_HOST, process.env.DB_NAME, process.env.DB_PORT);

require('models')(db);

module.exports = db;
