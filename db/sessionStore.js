const db = require('db');

const CONSTANTS = require('const');
const SessionStore = db.model(CONSTANTS.MODELS.SESSION_STORE);

class _SessionStore {
    constructor() {
    }

    async get(key, maxAge, {rolling}) {
        const sessionStoreModel = await SessionStore.findById(key);

        return sessionStoreModel && sessionStoreModel.toJSON();
    }

    async set(key, sess, maxAge, {rolling, changed}) {
        const sessionStore = {
            _id   : key,
            userId: sess.userId
        };
        const sessionStoreModel = new SessionStore(sessionStore);

        await sessionStoreModel.save();
    }

    async destroy(key) {
        await  SessionStore.remove({_id: key});
    }
}

module.exports = new _SessionStore();
