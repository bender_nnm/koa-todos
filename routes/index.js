module.exports = () => {
    const Router = require('koa-router');

    const errorHandler = require('helpers/errorHandler');

    const route = new Router();
    const authRoute = require('./auth')();
    const todoRoute = require('./todo')();

    route.use(errorHandler);

    route.use('/auth', authRoute.routes());
    route.use('/todos', todoRoute.routes());

    route.get('/', async (ctx, next) => {
        ctx.body = 'Root route!';
    });

    return route.routes();
};
