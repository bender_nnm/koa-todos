module.exports = () => {
    const Router = require('koa-router');
    const TodoHandler = require('controllers/todo');

    const route = new Router();
    const todoHandler = new TodoHandler();

    route.get('/', todoHandler.fetch);
    route.get('/:id', todoHandler.fetchById);
    route.post('/', todoHandler.createTodo);
    route.patch('/:id', todoHandler.updateTodo);
    route.patch('/state/:id', todoHandler.updateState);
    route.delete('/:id', todoHandler.deleteTodo);

    return route;
};
