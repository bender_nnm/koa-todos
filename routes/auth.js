module.exports = () => {
    const Router = require('koa-router');
    const AuthHandler = require('controllers/auth');
    const sessionHelper = require('helpers/session');
    const passwordHelper = require('helpers/password');

    const route = new Router();
    const authHandler = new AuthHandler();

    route.post('/signup', passwordHelper.signUp, authHandler.signUp);
    route.post('/signin', passwordHelper.signIn, authHandler.signIn);
    route.get('/signout', sessionHelper.authentication, authHandler.signOut);
    route.get('/authentication', sessionHelper.authentication, authHandler.authentication);

    return route;
};
