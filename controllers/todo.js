const db = require('db');

const CONSTANTS = require('const');
const Todo = db.model(CONSTANTS.MODELS.TODO);

class TodoHandler {

    constructor() {
    }

    // todo add filter functionality
    async fetch(ctx, next) {
        const todos = await Todo.find({}, {_id: 1, title: 1, rating: 1, finished: 1});

        ctx.status = 200;
        ctx.body = todos;
    }

    async fetchById(ctx, next) {
        const {id} = ctx.params;
        const todo = await Todo.findById(id, {_id: 1, title: 1, rating: 1, finished: 1});

        if (!todo) {
            ctx.throw(404, 'Todo did not find.');
        }

        ctx.status = 200;
        ctx.body = todo;
    }

    async createTodo(ctx, next) {
        const {title, rating} = ctx.request.body;

        if (!title) {
            ctx.throw(400, 'Title is required!');
        }

        if (!rating) {
            ctx.throw(400, 'Rating is required!');
        }

        if (rating < 0 || rating > 5) {
            ctx.throw(400, 'Rating is invalid!');
        }

        const todo = {title, rating};
        const todoModel = new Todo(todo);

        const id = todoModel.get('_id');

        await todoModel.save();

        ctx.status = 201;
        ctx.body = {id, title, rating};
    }

    async updateTodo(ctx, next) {
        let updateTodo = {};
        const {id} = ctx.params;
        const {title, rating} = ctx.request.body;

        if (!title && !rating) {
            ctx.status = 200;
            return ctx.body = {message: 'Nothing to update.'};
        }

        if (title) {
            updateTodo.title = title;
        }

        if (rating) {
            updateTodo.rating = rating;
        }

        const todoModel = await Todo.findById(id);

        if (!todoModel) {
            ctx.throw(404, 'Todo did not find.')
        }

        todoModel.set(updateTodo);

        await todoModel.save();

        ctx.status = 200;
        ctx.body = {
            title : todoModel.get('title'),
            rating: todoModel.get('rating')
        };
    }

    async updateState(ctx, next) {
        const id = ctx.params.id;
        const body = ctx.request.body;
        const finished = !!body.finished;

        const todoModel = await Todo.findById(id);

        if (!todoModel) {
            ctx.throw(404, 'Todo did not find.');
        }

        todoModel.set('finished', finished);
        todoModel.set('updateAt', new Date());

        await todoModel.save();

        ctx.status = 200;
        ctx.body = {finished};
    }

    async deleteTodo(ctx, next) {
        const {id} = ctx.params;
        const todo = await Todo.findById(id);

        if (!todo) {
            ctx.throw(404, 'Todo did not find.');
        }

        await Todo.remove({_id:id});

        ctx.status = 200;
        ctx.body = {message: 'Todo was removed successfully.'};
    }

}

module.exports = TodoHandler;
