const db = require('db');

const CONSTANTS = require('const');
const User = db.model(CONSTANTS.MODELS.USER);

class AuthHandler {

    constructor() {
    }

    async signUp(ctx, next) {
        const {name, email, password} = ctx.request.body;

        if (!name || !email) {
            ctx.throw(400, 'Please send all required fields.');
        }

        const user = {name, email, password};
        const userModel = new User(user);

        const count = await User.count({email});

        if (count) {
            ctx.throw(400, 'Invalid email.');
        }

        await userModel.save();

        ctx.status = 201;
        ctx.body = {message: 'User was created successfully.'};
    }

    async signIn(ctx, next) {
        const {email, password} = ctx.request.body;

        if (!email) {
            ctx.throw(400, 'Please send all required fields.');
        }

        const user = await User.findOne({email, password}, {name: 1}).lean();

        if (!user) {
            ctx.throw(400, 'User didn\'t find or invalid params.')
        }

        ctx.session.userId = user._id.toString();

        ctx.status = 200;
        ctx.body = {name: user.name};
    }

    async signOut(ctx, next) {
//        ctx.session.destroy();

        ctx.session = null;

        ctx.status = 200;
        ctx.body = {message: 'sign out'};
    }

    async authentication(ctx, next) {
        const userId = ctx.session.userId;
        const user = await User.findById(userId, {name: 1}).lean();

        if (!user) {
            ctx.throw(404, 'User didn\'t find.')
        }

        ctx.status = 200;
        ctx.body = {name: user.name};
    }
}

module.exports = AuthHandler;
