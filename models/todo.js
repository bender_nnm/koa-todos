const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CONSTANTS = require('const');

module.exports = db => {
    const schema = new Schema({
        title: {
            type    : String,
            required: true
        },

        rating: {
            type: Number
        },

        finished: {
            type   : Boolean,
            default: false
        },

        createdAt: {
            type   : Date,
            default: Date.now
        },

        updateAt: {
            type   : Date,
            default: Date.now
        }
    }, {
        collection: CONSTANTS.COLLECTION.TODOS
    });

    db.model(CONSTANTS.MODELS.TODO, schema);
};
