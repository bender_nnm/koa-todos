const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CONSTANTS = require('const');

module.exports = db => {
    const schema = new Schema({
        email: {
            type    : String,
            required: true,
            unique  : true
        },

        name: {
            type    : String,
            required: true
        },

        password: {
            type    : String,
            required: true
        },

        createdAt: {
            type   : Date,
            default: Date.now
        },

        updateAt: {
            type   : Date,
            default: Date.now
        }
    }, {
        collection: CONSTANTS.COLLECTION.USERS
    });

    db.model(CONSTANTS.MODELS.USER, schema);
};