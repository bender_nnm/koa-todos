module.exports = db => {
    require('./todo')(db);
    require('./user')(db);
    require('./sessionStore')(db);
};
