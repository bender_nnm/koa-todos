const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CONSTANTS = require('const');

module.exports = db => {
    const schema = new Schema({

        _id: {
            type   : String,
            require: true
        },

        userId: {
            type   : String,
            require: true
        },

        createdAt: {
            type   : Date,
            default: Date.now
        },

        updateAt: {
            type   : Date,
            default: Date.now
        }
    }, {
        collection: CONSTANTS.COLLECTION.SESSION_STORE
    });

    db.model(CONSTANTS.MODELS.SESSION_STORE, schema);
};
