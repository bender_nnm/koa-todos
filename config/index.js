process.env.DB_HOST = 'localhost';
process.env.DB_NAME = 'todos';
process.env.DB_PORT = 27017;

process.env.HOST = 'localhost:5657';
process.env.WEB_HOST = 'http://localhost:5657';
process.env.PORT = '5657';

process.env.CRYPTO_SECRET = 'crypto_secret';

module.exports = {
    db        : {
        port: 27017,
        name: 'todos',
        host: 'localhost'
    },
    server    : {
        port   : '5657',
        host   : 'localhost:5657',
        webHost: 'http://localhost:5657'
    },
    bodyParser: {
        jsonLimit : '32kb',
        urlencoded: true
    },
    session   : function (sessionStore) {
        return {
            store : sessionStore,
            maxAge: 5 * 60 * 1000,
            key   : '342ddsadfhtr45676756'
        }
    }
};
