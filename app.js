const db = require('./db');
const config = require('./config');

module.exports = async () => {
    db.on('error', console.error.bind(console, 'connection error:'));

    db.once('open', function () {
        const Koa = require('koa');
        const http = require('http');
        const sessionStore = require('./db/sessionStore');

        const logger = require('koa-logger');
        const compose = require('koa-compose');
        const session = require('koa-session');
        const bodyParser = require('koa-body');
        const allowCrossDomain = require('./helpers/allowCrossDomain');

        const app = new Koa();
        const server = http.createServer(app.callback());

        const routes = require('./routes')();
        const middleWares = compose([
            allowCrossDomain,
            logger(),
            session(config.session(sessionStore), app),
            bodyParser(config.bodyParser)
        ]);

        app.db = db;
        app.keys = ['somethingkeydontnowwhatittodo'];

        app.use(middleWares);
        app.use(routes);

        server.listen(process.env.port, function () {
            console.log('server started on ' + process.env.port + ' port');
        });
    });
};
