class SessionHelper {
    constructor() {
    }

    async authentication(ctx, next) {
        if (!ctx.session || !ctx.session.userId) {
            ctx.throw(401, 'Unauthorized.')
        }

        await  next();
    }
}

module.exports = new SessionHelper();
