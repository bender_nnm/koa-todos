const defaultOpts = {
    status : 400,
    message: 'Something wrong!',
    name   : 'Custom error'
};

class CustomError extends Error {
    constructor(opts) {
        super();
        super.captureStackTrace();

        opts || (opts = {});

        this.name = opts.name || defaultOpts.name;
        this.status = opts.status || defaultOpts.status;
        this.message = opts.message || defaultOpts.message;
    }
}

module.exports = CustomError;