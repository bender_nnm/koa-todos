module.exports = async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        ctx.status = err.statusCode || err.status || 500;
        ctx.body = {
            name   : err.name || 'Bad request',
            message: err.message
        };
    }
};

