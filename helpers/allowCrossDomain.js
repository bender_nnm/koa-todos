module.exports = async (ctx, next) => {
    const browser = ctx.request.get('user-agent');


    ctx.response.set('Access-Control-Allow-Credentials', 'true');
    ctx.response.set('Access-Control-Allow-Headers', ["Origin", "X-Requested-With", "Cookie", "Content-Type", "Accept", "X-Access-Token"]);
    ctx.response.set('Access-Control-Allow-Methods', 'GET,HEAD,PUT,OPTIONS,PATCH,POST,DELETE');
    ctx.response.set('Access-Control-Allow-Origin', 'http://localhost:4200');

    if (/Trident/.test(browser)) {
        ctx.response.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    }

    if (ctx.request.method === 'OPTIONS') {
        ctx.status = 200;
        return ctx.body = 'jkjk';
    }

    await next();
};