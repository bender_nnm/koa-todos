const crypto = require('crypto');

function encrypting(password) {
    const secret = process.env.CRYPTO_SECRET || 'crypto_secret';

    return crypto
        .createHmac('sha256', secret)
        .update(password)
        .digest('hex');
}

class PasswordHelper {
    constructor() {
    }

    async signUp(ctx, next) {
        const {password, confirmPassword} = ctx.request.body;

        if (!password || !confirmPassword) {
            ctx.throw(400, 'Please send all required fields.');
        }

        if (password !== confirmPassword || password.length < 6) {
            ctx.throw(400, 'Invalid password.');
        }

        ctx.request.password = encrypting(password);

        delete ctx.request.confirmPassword;

        await next();
    }

    async signIn(ctx, next) {
        const {password} = ctx.request.body;

        if (!password) {
            ctx.throw(400, 'Please send all required fields.');
        }

        if (password.length < 6) {
            ctx.throw(400, 'Invalid password.');
        }

        ctx.request.password = encrypting(password);

        await next();
    }
}

module.exports = new PasswordHelper();
