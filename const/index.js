module.exports = {
    VALIDATION: {
        PASSWORD    : /.{6,}/,
        EMAIL_REGEXP: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        OBJECT_ID   : /^[0-9a-fA-F]{24}$/
    },
    COLLECTION: {
        TODOS        : 'todos',
        USERS        : 'users',
        SESSION_STORE: 'session_store'
    },
    MODELS    : {
        TODO         : 'Todo',
        USER         : 'User',
        SESSION_STORE: 'Session_store'
    }
};
